var express = require("express");
var bodyParser = require("body-parser");
var mysql = require("mysql");

var app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "igdefault",
    database: "employees",
    connectionLimit: 4
});

const INSERTSQL = "insert into employees (first_name, last_name, gender, birth_date, " +
    "hire_date) values (?,?,?,?,?)";

var Employee = function (first_name, last_name, gender, hire_date, birth_date) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.gender = gender;
    this.birth_date = birth_date;
    this.hire_date = hire_date;
};

Employee.prototype.save = function (successCallback, errorCallback) {
    console.log("Saving Employee...");
    var self = this;

    pool.getConnection(function (err, connection) {
        if (err) {
            return errorCallback(err);
        }
        connection.query(
            INSERTSQL,
            [self.first_name, self.last_name, self.gender, self.birth_date, self.hire_date],
            function (err, result) {
                connection.release();
                if (err) {
                    return errorCallback(err);
                }
                successCallback(result)
            });
    });
};

app.post("/api/employee/save", function (req, res) {
    var gender = req.body.gender == 'male' ? 'M' : 'F'; //if enum fields
    var birthDate = req.body.birthday.substring(0, req.body.birthday.indexOf('T'));
    var hireDate = req.body.hiredate.substring(0, req.body.hiredate.indexOf('T'));

    var employee = new Employee(
        req.body.firstname,
        req.body.lastname,
        gender,
        birthDate,
        hireDate
    );

    console.log("New Employee", employee);

    employee.save(function success(result) {

        console.log("Success");
        res.status(202).json({url: "/api/employee/" + result.insertId});

    }, function error(err) {

        console.log("error");
        res.status(500).end();

        console.log("Error Occurred", err);
    })

});

app.use(express.static(__dirname + "/public"));

app.listen(3000, function () {
    console.info("App Server started on port 3000");
});


